# TP2 : Gestion de service




# Sommaire

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis






# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`
````
[ad@web ~]$ sudo dnf install httpd
````
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

> Ce que j'entends au-dessus par "fichier de conf principal" c'est que c'est **LE SEUL** fichier de conf lu par Apache quand il démarre. C'est souvent comme ça : un service ne lit qu'un unique fichier de conf pour démarrer. Cherchez pas, on va toujours au plus simple. Un seul fichier, c'est simple.  
**En revanche** ce serait le bordel si on mettait toute la conf dans un seul fichier pour pas mal de services.  
Donc, le principe, c'est que ce "fichier de conf principal" définit généralement deux choses. D'une part la conf globale. D'autre part, il inclut d'autres fichiers de confs plus spécifiques.  
On a le meilleur des deux mondes : simplicité (un seul fichier lu au démarrage) et la propreté (éclater la conf dans plusieurs fichiers).

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
  ````
    [ad@web ~]$ sudo systemctl start httpd.service
  ````
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
  ````
  [ad@web ~]$ sudo systemctl enable httpd.service

    Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
  ````
  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
    ````
    [ad@web ~]$ sudo ss -lntp | grep httpd

    LISTEN 0      511                *:80              *:*    users:(("httpd",pid=10808,fd=4),("httpd",pid=10807,fd=4),("httpd",pid=10806,fd=4),("httpd",pid=10804,fd=4))
    `````
    - une portion du mémo commandes est dédiée à `ss`
````
[ad@web ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp

success
````


🌞 **TEST**

- vérifier que le service est démarré
````
[ad@web ~]$ sudo systemctl status httpd.service

● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabl>
     Active: active (running) since Fri 2022-11-18 16:19:13 CET; 11min ago
       Docs: man:httpd.service(8)
   Main PID: 10804 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec>
      Tasks: 213 (limit: 5907)
     Memory: 23.2M
        CPU: 264ms
     CGroup: /system.slice/httpd.service
             ├─10804 /usr/sbin/httpd -DFOREGROUND
             ├─10805 /usr/sbin/httpd -DFOREGROUND
             ├─10806 /usr/sbin/httpd -DFOREGROUND
             ├─10807 /usr/sbin/httpd -DFOREGROUND
             └─10808 /usr/sbin/httpd -DFOREGROUND

Nov 18 16:19:13 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 18 16:19:13 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 18 16:19:13 web.tp2.linux httpd[10804]: Server configured, listening on: port 80
````
- vérifier qu'il est configuré pour démarrer automatiquement
````
[ad@web ~]$ systemctl list-unit-files | grep enabled | grep httpd

httpd.service                              enabled         disabled
````
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[ad@web ~]$ curl localhost

<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
````
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
````
👍
````

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
````
[ad@web ~]$ cat /etc/systemd/system/multi-user.target.wants/httpd.service

# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
````

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
````
[ad@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User

User apache
````
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
````
[ad@web ~]$ ps -ef | grep apache

apache     10805   10804  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10806   10804  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10807   10804  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     10808   10804  0 16:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ad         11140     860  0 17:13 pts/0    00:00:00 grep --color=auto apache
````
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf
````
[ad@web ~]$ ls -al /usr/share/testpage/

total 12
drwxr-xr-x.  2 root root   24 Nov 18 16:10 .
drwxr-xr-x. 82 root root 4096 Nov 18 16:10 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
````

🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
````
[ad@web ~]$ sudo useradd toto -d /usr/share/httpd -s /sbin/nologin

useradd: warning: the home directory /usr/share/httpd already exists.
useradd: Not copying any file from skel directory into it.

[ad@web ~]$ cat /etc/passwd | grep toto

toto:x:1001:1001::/usr/share/httpd:/sbin/nologin
````
- redémarrez Apache
````
[ad@web ~]$ sudo systemctl restart httpd
````
- utilisez une commande `ps` pour vérifier que le changement a pris effet
````
[ad@web ~]$ ps -ef | grep apache

apache     12038   12037  0 00:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     12039   12037  0 00:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     12040   12037  0 00:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     12041   12037  0 00:53 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
ad         12286     860  0 01:08 pts/0    00:00:00 grep --color=auto apache
````

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix
````
[ad@web ~]$ sudo cat /etc/httpd/conf/httpd.conf |grep 4242

Listen 4242
````
- ouvrez ce nouveau port dans le firewall, et fermez l'ancien
````
[ad@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent

Warning: NOT_ENABLED: 80:tcp
success

[ad@web ~]$ sudo firewall-cmd --add-port=4242/tcp --permanent

success

[ad@web ~]$ sudo firewall-cmd --reload

success
````
- redémarrez Apache
````
[ad@web ~]$ sudo systemctl restart httpd
````
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
````
[ad@web ~]$ sudo ss -alnpt |grep 4242

LISTEN 0      511                *:4242            *:*    users:(("httpd",pid=12350,fd=4),("httpd",pid=12349,fd=4),("httpd",pid=12348,fd=4),("httpd",pid=12346,fd=4))
````
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
````
[ad@web ~]$ curl localhost:4242

<!doctype html>
...
````
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
````
👍
````

📁 **Fichier `/etc/httpd/conf/httpd.conf`**
````

ServerRoot "/etc/httpd"

Listen 4242

Include conf.modules.d/*.conf

User apache
Group apache


ServerAdmin root@localhost


<Directory />
    AllowOverride none
    Require all denied
</Directory>


DocumentRoot "/var/www/html"

<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>

<Directory "/var/www/html">
    Options Indexes FollowSymLinks

    AllowOverride None

    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>

<Files ".ht*">
    Require all denied
</Files>

ErrorLog "logs/error_log"

LogLevel warn

<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common

    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>


    CustomLog "logs/access_log" combined
</IfModule>

<IfModule alias_module>


    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"

</IfModule>

<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>

<IfModule mime_module>
    TypesConfig /etc/mime.types

    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz



    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>

AddDefaultCharset UTF-8

<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>


EnableSendfile on

IncludeOptional conf.d/*.conf
````

# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
````
[ad@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[ad@web ~]$ sudo firewall-cmd --remove-port=4242/tcp --permanent
success
[ad@web ~]$ sudo firewall-cmd --reload
success
[ad@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[ad@web ~]$ sudo systemctl restart httpd
[ad@web ~]$ sudo cat /etc/httpd/conf/httpd.conf |grep 80
Listen 80
````
- reprendre l'utilisateur par défaut

## 1. Intro blabla

**Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients.** C'est sur son IP que les clients devront aller pour visiter le site web.  

**Le service de base de données `db.tp2.linux` sera uniquement accessible depuis `web.tp2.linux`.** Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon le but de ce TP est juste de s'exercer à faire tourner des services, un serveur + sa base de données, c'est un peu le cas d'école. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment, on va se contenter d'une conf minimale.

Je vais pas vous demander de coder une application, et cette fois on se contentera pas d'un simple `index.html` tout moche et on va se mettre dans la peau de l'admin qui se retrouve avec une application à faire tourner. **On va faire tourner un [NextCloud](https://nextcloud.com/).**

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

---

Le flow va être le suivant :

➜ **on prépare d'abord la base de données**, avant de setup NextCloud

- comme ça il aura plus qu'à s'y connecter
- ce sera sur une nouvelle machine `db.tp2.linux`
- il faudra installer le service de base de données, puis lancer le service
- on pourra alors créer, au sein du service de base de données, le nécessaire pour NextCloud

➜ **ensuite on met en place NextCloud**

- on réutilise la machine précédente avec Apache déjà installé, ce sera toujours Apache qui accueillera les requêtes des clients
- mais plutôt que de retourner une bête page HTML, NextCloud traitera la requête
- NextCloud, c'est codé en PHP, il faudra donc **installer une version de PHP précise** sur la machine
- on va donc : install PHP, configurer Apache, récupérer un `.zip` de NextCloud, et l'extraire au bon endroit !

![NextCloud install](./pics/nc_install.png)

## 2. Setup

🖥️ **VM db.tp2.linux**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- je veux dans le rendu **toutes** les commandes réalisées
````
[ad@localhost ~]$ sudo dnf install mariadb-server
[ad@localhost ~]$ sudo systemctl enable mariadb
[ad@localhost ~]$ sudo mysql_secure_installation
````
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
````
[ad@localhost ~]$ sudo ss -alnpt | grep mariadbd
LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=3170,fd=19))
````
  - il sera nécessaire de l'ouvrir dans le firewall
  ````
  [ad@localhost ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[ad@localhost ~]$ sudo firewall-cmd --reload
success
  ````

> La doc vous fait exécuter la commande `mysql_secure_installation` c'est un bon réflexe pour renforcer la base qui a une configuration un peu *chillax* à l'install.

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
  - exécutez les commandes SQL suivantes :

```sql
-- Création d'un utilisateur dans la base, avec un mot de passe
-- L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
-- Dans notre cas, c'est l'IP de web.tp2.linux
-- "pewpewpew" c'est le mot de passe hehe
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'ad';

-- Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

-- Actualisation des privilèges
FLUSH PRIVILEGES;

-- C'est assez générique comme opération, on crée une base, on crée un user, on donne les droits au user sur la base
```

> Par défaut, vous avez le droit de vous connecter localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
    - si vous ne l'avez pas, installez-là
    - vous pouvez déterminer dans quel paquet est disponible la commande `mysql` en saisissant `dnf provides mysql`
- **donc vous devez effectuer une commande `mysql` sur `web.tp2.linux`**
````
ad@web ~]$ sudo mysql -u nextcloud -h 10.102.1.12 -p
````
- une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
mysql> SHOW DATABASES;

+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;

Database changed

mysql> SHOW TABLES;

Empty set (0.01 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

- vous ne pourrez pas utiliser l'utilisateur `nextcloud` de la base pour effectuer cette opération : il n'a pas les droits
````
MariaDB [(none)]> SELECT User FROM mysql.user;

+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
4 rows in set (0.001 sec)

mysql> exit

Bye
````
- il faudra donc vous reconnectez localement à la base en utilisant l'utilisateur `root`

> Les utilisateurs de la base de données sont différents des utilisateurs du système Rocky Linux qui porte la base. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

Une fois qu'on s'est assurés qu'on peut se co au service de base de données depuis `web.tp2.linux`, on peut continuer.

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
$ sudo dnf config-manager --set-enabled crb
# On ajoute le dépôt REMI
$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
$ dnf module list php

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
$ sudo dnf module enable php:remi-8.1 -y

# Eeeet enfin, on installe la bonne version de PHP : 8.1
$ sudo dnf install -y php81-php
```
````
Complete!
````

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```
````
Complete!
````

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`
````
[ad@web ~]$ sudo mkdir /var/www/tp2_nextcloud/
````
  - ce sera notre *racine web* (ou *webroot*)
  - l'endroit où le site est stocké quoi, on y trouvera un `index.html` et un tas d'autres marde, tout ce qui constitue NextClo :D
- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
````
[ad@web ~]$ sudo dnf install -y wget

[ad@web ~]$ wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
````
- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`
  - installez la commande `unzip` si nécessaire
  ````
  [ad@web ~]$ sudo dnf install unzip
  ````
  - vous pouvez extraire puis déplacer ensuite, vous prenez pas la tête
  ````
    [ad@web ~]$ sudo unzip nextcloud-25.0.0rc3.zip -d /var/www/tp2_nextcloud/
    #matrix 

    [ad@web ~]$ sudo mv /var/www/tp2_nextcloud/nextcloud/* /var/www/tp2_nextcloud/

    [ad@web ~]$ sudo rm -r /var/www/tp2_nextcloud/nextcloud/

  ````
  - contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place
  ````
    [ad@web ~]$ sudo ls /var/www/tp2_nextcloud/ | grep index.html
    index.html
  ````
- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache
````
[ad@web ~]$ sudo chown apache -R /var/www/tp2_nextcloud/
[ad@web ~]$ sudo chgrp apache -R /var/www/tp2_nextcloud/
[ad@web ~]$ ls -l /var/www/ | grep tp2_nextcloud
drwxr-xr-x. 14 apache apache 4096 Nov 28 23:03 tp2_nextcloud
````

> A chaque fois que vous faites ce genre de trucs, assurez-vous que c'est bien ok. Par exemple, vérifiez avec un `ls -al` que tout appartient bien à l'utilisateur qui exécute Apache.

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
````
[ad@web ~]$ cat /etc/httpd/conf/httpd.conf | tail -n 1

IncludeOptional conf.d/*.conf
````
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

```apache
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```
````
[ad@web ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf

[ad@web ~]$ cat /etc/httpd/conf.d/nextcloud.conf

<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
````

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf
````
[ad@web ~]$ sudo systemctl restart httpd

[ad@web ~]$ systemctl is-active httpd

active
````

### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`
- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
````
[ad@db ~]$ sudo mysql -u root -p
````
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL
````
MariaDB [(none)]> USE nextcloud;

MariaDB [nextcloud]> SELECT FOUND_ROWS();

+--------------+
| FOUND_ROWS() |
+--------------+
|          124 |
+--------------+
1 row in set (0.000 sec)

````
