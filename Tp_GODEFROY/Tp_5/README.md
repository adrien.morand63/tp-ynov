

# I. Setup DB
## 1. Install MariaDB
````
[AD@db ~]$ sudo dnf install mariadb-server
[...]
Complete!
````
````
[AD@db ~]$ systemctl start mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'mariadb.service'.
Authenticating as: adrien (AD)
Password:
==== AUTHENTICATION COMPLETE ====
````

````
[AD@db ~]$ systemctl statu mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 12:20:18 CET; 1min 7s ago
[...]
````
````
[AD@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
````
````
[AD@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 12:20:18 CET; 4min 47s ago
[...]
````
````
[AD@db ~]$ ss -laputn | grep 10.5.1.12
tcp   ESTAB  0      36            10.5.1.12:22      10.5.1.69:63551
````
````
[AD@db ~]$ ps -ef | grep mariadb
AD          5014    1688  0 12:34 pts/0    00:00:00 grep --color=auto mariadb
````
````
[AD@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
````
## 2. Conf MariaDB

````
[AD@db ~]$ mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
````
````
[AD@db ~]$ sudo mysql -u root -p
[sudo] password for AD:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

````

````
[AD@web ~]$ dnf provides mysql
Rocky Linux 8 - AppStream                                               2.2 MB/s | 8.2 MB     00:03
Rocky Linux 8 - BaseOS                                                  2.7 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                                                   22 kB/s |  10 kB     00:00
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : @System
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7

mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
````
````
[AD@web ~]$ mysql -h 10.5.1.12 -P 3306 -u 'nextcloud' -p nextcloud
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 22
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
````
````
mysql> SHOW TABLES;
Empty set (0.00 sec)
````
````
mysql> exit
Bye
````
# II. Setup Web
## 1. Install Apache
````
[AD@web ~]$ sudo dnf install httpd
[...]
Complete!
````
````
[AD@web ~]$ sudo systemctl start httpd
[AD@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[AD@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-26 15:13:34 CET; 28s ago
[...]
````
````
[AD@web ~]$ sudo ss -laput | grep httpd
tcp   LISTEN 0      128                   *:http              *:*      users:(("httpd",pid=2799,fd=4),(
httpd",pid=2798,fd=4),("httpd",pid=2797,fd=4),("httpd",pid=2795,fd=4))
````
````
[AD@web ~]$ ps -aux | grep httpd
root        2795  0.0  0.3 282936 11480 ?        Ss   15:13   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2796  0.0  0.2 296820  8496 ?        S    15:13   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2797  0.0  0.3 1485732 14264 ?       Sl   15:13   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2798  0.0  0.3 1354604 12216 ?       Sl   15:13   0:00 /usr/sbin/httpd -DFOREGROUND
apache      2799  0.0  0.3 1354604 12216 ?       Sl   15:13   0:00 /usr/sbin/httpd -DFOREGROUND
AD          3047  0.0  0.0 221928  1148 pts/0    S+   15:19   0:00 grep --color=auto httpd
````
````
[AD@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[AD@web ~]$ sudo firewall-cmd --reload
success
````
````
[AD@web ~]$ curl 10.5.1.11
<!doctype html>
[gros bordel html]
</html>
````
````
[AD@web ~]$ sudo dnf install epel-release
[AD@web ~]$ sudo dnf update
[AD@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[AD@web ~]$ sudo dnf module enable php:remi-7.4
[AD@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
(pour chaque) Complete!
````
## 2. Conf Apache
````
[AD@web ~]$ grep conf.d /etc/httpd/conf/httpd.conf
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
````
````
[AD@web ~]$ cd /etc/httpd/conf.d/
[AD@web conf.d]$ sudo touch virtualhost.conf
[AD@web conf.d]$ sudo nano virtualhost.conf
[AD@web conf.d]$ cat virtualhost.conf
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
````
````
[AD@web conf.d]$ sudo mkdir -p /var/www/nextcloud/html/
[AD@web conf.d]$ sudo chown -R apache:apache /var/www
````
````
[AD@web conf.d]$ timedatectl
               Local time: Sat 2021-11-27 06:37:24 CET
           Universal time: Sat 2021-11-27 05:37:24 UTC
                 RTC time: Sat 2021-11-27 04:14:59
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: active
          RTC in local TZ: no
[AD@web conf.d]$ sudo nano /etc/opt/remi/php74/php.ini
[AD@web conf.d]$ sudo cat /etc/opt/remi/php74/php.ini
[PHP]

;;;;;;;;;;;;;;;;;;;
; About php.ini   ;
;;;;;;;;;;;;;;;;;;;
````
````
[AD@web conf.d]$ cat /etc/opt/remi/php74/php.ini | grep "Europe/Paris"
;date.timezone = "Europe/Paris"
````
````

````
