# TP2 : Ethernet, IP, et ARP




## I. Setup IP
---

```
netsh interface ipv4 set address name="Ethernet" static 192.168.10.1 255.255.255.192 192.168.10.2

```
```
AD > 192.168.10.1 /26
RAD > 192.168.10.2 /26

adresse resseaux > 192.168.10.0
Adresse Broadcast > 192.168.10.63



```
```
PS C:\Users\adrie> ping 192.168.10.2

Envoi d’une requête 'Ping'  192.168.10.2 avec 32 octets de données :
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
```

wireshark ping.pcapng


## II. 5 Interlude hackerzz
---

```
PS C:\Users\adrie> arp -a

Interface : 192.168.10.1 --- 0x14

  Adresse Internet      Adresse physique   

binome:

  192.168.10.2          fc-34-97-00-38-84

gateway :

  10.33.19.254          00-c0-e7-e0-04-4e

```
```
PS C:\WINDOWS\system32> arp -d
PS C:\WINDOWS\system32> arp -a

Interface : 10.200.1.1 --- 0x3
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.33.18.127 --- 0xa
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 10.5.1.69 --- 0x12
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.10.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

```
```
PS C:\WINDOWS\system32> ping 192.168.10.2

Envoi d’une requête 'Ping'  192.168.10.2 avec 32 octets de données :
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128

  PS C:\WINDOWS\system32> arp -a

Interface : 10.200.1.1 --- 0x3
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.18.127 --- 0xa
  Adresse Internet      Adresse physique      Type
  10.33.19.254          00-c0-e7-e0-04-4e     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.5.1.69 --- 0x12
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 192.168.10.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.10.2          fc-34-97-00-38-84     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Wireshark arp.pcapng


## III. DHCP you too my brooo
---

Pour cet exercice après approximativement 500 essai je n'arrive toujours pas a avoir l'échange dhcp Dora au complet même avec l'aide de certains qui ont réussi. j'ai au maximum eu le ra de Dora du coup baaaah oe merci Windows et sa magie obscure je suppose.


## IV. Avant-goût TCP et UDP
---

Wireshark youtub.pcapng

- Source Address: 2a02:8400:1:213::d 
- Source Port: 443