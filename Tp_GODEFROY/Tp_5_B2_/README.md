# Scanner_reseau
Recuperation:
````
git clone https://github.com/Affy657/Scanner_reseau.git
````


Cet outillé permet de lister les IP active sur le réseaux à partire d'une plage d'IP entrer en paramètre puis de lister les ports ouvre pour chacune

---

Execution de Scanner_ip.py:
````
$ py Scanner_ip.py [options] 
  options : -h, -i, -t, -o, -n

  Exemples :
          py Scanner_ip.py --help

          py Scanner_ip.py -i 10.33.16.1-254
````
Aprer sela vous recuperer un fichier log_Scanner_ip.txt contenen toute les adreese ip active
vous pouvez ensuite exécuter Scanner Portsspy pour récupérer les ports ouvre sur les Ip

---
Execution de Scanner_ports.py:
````
$ py Scanner_ports.py
Enter adresse IP a scanner : [adresse ip]
````
Exemples :
````
$ py Scanner_ports.py
Enter adresse IP a scanner : 10.33.16.47
[+]      demarage du scanne de:  10.33.16.47
[+]      139 is open
[+]      135 is open
[+]      445 is open
[+]      Time taken: 1.3543031215667725
````

