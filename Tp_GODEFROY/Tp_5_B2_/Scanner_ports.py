import socket
import time
import threading

from queue import Queue
socket.setdefaulttimeout(0.25)
print_lock = threading.Lock()

target = input('Enter adresse IP a scanner : ' )

t_IP = socket.gethostbyname(target)
print ("[+]\t",'demarage du scanne de: ', t_IP)

def portscan(port):
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   try:
      con = s.connect((t_IP, port))
      with print_lock:
         print("[+]\t", port, 'is open')
      con.close()
   except:
     pass

def threader():
   while True:
      worker = q.get()
      portscan(worker)
      q.task_done()

q = Queue()
startTime = time.time()

for x in range(100):
   t = threading.Thread(target = threader)
   t.daemon = True
   t.start()

for worker in range(1, 500):
   q.put(worker)

q.join()
print("[+]\t",'Time taken:', time.time() - startTime)