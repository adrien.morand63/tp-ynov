


> Written with [StackEdit](https://stackedit.io/).
# I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale


### En ligne de commande  

```C:\Users\adrie>ipconfig /all

Carte réseau sans fil Wi-Fi :   
Description. . . . . . . . . . . . . . : Killer(R) Wi-Fi 6 AX1650x 160MHz Wireless Network Adapter (200NGW)
Adresse physique . . . . . . . . . . . : 38-00-25-AB-51-49
Adresse IPv4. . . . . . . . . . . . . .: 10.33.18.127(préféré)
```
```
Carte Ethernet Ethernet :
Description. . . . . . . . . . . . . . : Killer E2500 Gigabit Ethernet Controller
Adresse physique . . . . . . . . . . . : 7C-D3-0A-82-6C-CE
Adresse IPv4. . . . . . . . . . . . . .: x
```
```
C:\Users\adrie>ipconfig /all

Passerelle par défaut. . . . . . . . . : 10.33.19.254
```
### En graphique (GUI : Graphical User Interface)


![AD](./Capture%20d%E2%80%99%C3%A9cran%202022-09-27%20164613.png )

![AD](./Capture%20d%E2%80%99%C3%A9cran%202022-09-27%20164629.png )

**Question:**
Pouvoir communiquer avec le réseaux

## 2. Modifications des informations

![AD]( ./Capture%20d’écran%202022-09-27%20165836.png)
![AD](./Capture%20d’écran%202022-09-27%20165910.png )
![AD](./Capture%20d’écran%202022-09-27%20165958.png )



**Question :**
On peut perdre l'accès a internet car le réseaux dans le quel on es ne reconnais plus notre ip


# II. Exploration locale en duo

 ![AD](./Capture%20d’écran%202022-09-28%20144318.png)
```
C:\Users\adrie>ipconfig

Carte Ethernet Ethernet :

 Adresse IPv4. . . . . . . . . . . . . .: 192.168.69.253
 Masque de sous-réseau. . . . . . . . . : 255.255.255.252
```
```
C:\Users\adrie>ping 192.168.69.254

Envoi d’une requête 'Ping'  192.168.69.254 avec 32 octets de données :
Réponse de 192.168.69.254 : octets=32 temps<1ms TTL=128
Réponse de 192.168.69.254 : octets=32 temps<1ms TTL=128
Délai d’attente de la demande dépassé.
Réponse de 192.168.69.254 : octets=32 temps=1 ms TTL=128

```
```
C:\Users\adrie>arp -a 192.168.69.254

Interface : 192.168.69.253 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.69.254        fc-34-97-00-38-84     dynamique

```
### Utilisation d'un des deux comme gateway
 ```
    PS C:\Users\Radwa> ping 1.1.1.1
    
    Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
    Réponse de 1.1.1.1 : octets=32 temps=21 ms TTL=54
    Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
    Réponse de 1.1.1.1 : octets=32 temps=28 ms TTL=54
    Réponse de 1.1.1.1 : octets=32 temps=29 ms TTL=54
   ```
```
C:\Users\adrie>tracert 192.168.137.2

Détermination de l’itinéraire vers LAPTOP-L066RDBA [192.168.137.2]
avec un maximum de 30 sauts :

  1     1 ms     1 ms    <1 ms  LAPTOP-L066RDBA [192.168.137.2]

Itinéraire déterminé.

```
```

C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888

```
```
PS C:\Users\Radwa\Desktop\netcat-1.11> .\nc.exe 192.168.137.1 8888 
cc 
Adrien 
Faut qu'on parle ta mere et moi 
vient dans la chambre fiston
```
```


C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888 192.168.137.2

PS C:\WINDOWS\system32> netstat -a -b -n | select-string 8888

  TCP    192.168.137.1:8888     0.0.0.0:0              LISTENING
  ```
  ```
C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888 127.0.0.1

PS C:\WINDOWS\system32> netstat -a -b -n | select-string 8888

  TCP    127.0.0.1:8888         0.0.0.0:0              LISTENING

```



![AD](./Capture%20d’écran%202022-09-28%20165329.png)
```
C:\Users\adrie>ping 192.168.137.2

Envoi d’une requête 'Ping'  192.168.137.2 avec 32 octets de données :
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.137.2 : octets=32 temps<1ms TTL=128

```
```

C:\Users\adrie\Downloads\netcat-win32-1.11\netcat-1.11>nc.exe -l -p 8888
Salut léo

```
```


C:\Users\adrie>ipconfig/all
Serveur DHCP . . . . . . . . . . . . . : 10.33.19.254
Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 14:49:29
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
```
```
C:\Users\adrie>nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8
```
```
C:\Users\adrie>nslookup ynov.com
Serveur :   dns.google
Address:  8.8.8.8
```
```
C:\Users\adrie>nslookup 78.74.21.21
Serveur :   dns.google
Address:  8.8.8.8
```
```
C:\Users\adrie>nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8
```


![AD](./Capture%20d’écran%202022-09-28%20170657.png)

jai plus mon camarade a coter pour le ping :'(

![AD](./Capture%20d’écran%202022-09-28%20173632.png)
![AD](./Capture%20d’écran%202022-09-28%20174204.png)

