


# Partie 2 : FTP

  
# II. Setup du serveur FTP

  
## 1. Installation du serveur

  ````
  adrien@node1:~/Bureau$ sudo apt install vsftpd  
[sudo] Mot de passe de adrien :  
Désolé, essayez de nouveau.  
[sudo] Mot de passe de adrien :  
Lecture des listes de paquets... Fait  
Construction de l'arbre des dépendances  
Lecture des informations d'état... Fait  
Les NOUVEAUX paquets suivants seront installés :  
vsftpd  
0 mis à jour, 1 nouvellement installés, 0 à enlever et 96 non mis à jour.  
Il est nécessaire de prendre 115 ko dans les archives.  
Après cette opération, 338 ko d'espace disque supplémentaires seront utilisés.  
Réception de :1 [http://fr.archive.ubuntu.com/ubuntu](http://fr.archive.ubuntu.com/ubuntu) focal/main amd64 vsftpd amd64 3.0.3-12 [115 kB]  
115 ko réceptionnés en 0s (765 ko/s)  
Préconfiguration des paquets...  
Sélection du paquet vsftpd précédemment désélectionné.  
(Lecture de la base de données... 206232 fichiers et répertoires déjà installés.)  
Préparation du dépaquetage de .../vsftpd_3.0.3-12_amd64.deb ...  
Dépaquetage de vsftpd (3.0.3-12) ...  
Paramétrage de vsftpd (3.0.3-12) ...  
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.  
Traitement des actions différées (« triggers ») pour man-db (2.9.1-1) ...  
Traitement des actions différées (« triggers ») pour systemd (245.4-4ubuntu3.11) ...
  ````
 ````
   
adrien@node1:~/Bureau$ systemctl start  
Too few arguments.  
adrien@node1:~/Bureau$ systemctl status  
● node1  
State: running  
Jobs: 0 queued  
Failed: 0 units  
Since: Fri 2021-11-05 17:31:01 CET; 19min ago  
CGroup: /  
├─user.slice  
│ └─user-1000.slice  
│ ├─user@1000.service  
│ │ ├─gvfs-goa-volume-monitor.service  
│ │ │ └─1085 /usr/libexec/gvfs-goa-volume-monitor  
│ │ ├─pulseaudio.service  
│ │ │ └─840 /usr/bin/pulseaudio --daemonize=no --log-target=journal  
│ │ ├─gvfs-daemon.service  
│ │ │ ├─ 950 /usr/libexec/gvfsd  
│ │ │ ├─ 955 /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f -o big_writes  
│ │ │ └─1117 /usr/libexec/gvfsd-trash --spawner :1.8 /org/gtk/gvfs/exec_spaw/0  
│ │ ├─gvfs-udisks2-volume-monitor.service  
│ │ │ └─1070 /usr/libexec/gvfs-udisks2-volume-monitor  
│ │ ├─xfce4-notifyd.service  
│ │ │ └─1047 /usr/lib/x86_64-linux-gnu/xfce4/notifyd/xfce4-notifyd  
│ │ ├─init.scope  
│ │ │ ├─834 /lib/systemd/systemd --user  
│ │ │ └─835 (sd-pam)  
│ │ ├─gvfs-gphoto2-volume-monitor.service  
│ │ │ └─1080 /usr/libexec/gvfs-gphoto2-volume-monitor  
│ │ ├─obex.service  
│ │ │ └─1165 /usr/lib/bluetooth/obexd  
│ │ ├─at-spi-dbus-bus.service  
│ │ │ ├─947 /usr/libexec/at-spi-bus-launcher  
│ │ │ ├─961 /usr/bin/dbus-daemon --config-file=/usr/share/defaults/at-spi2/accessibility.conf --n>  
│ │ │ └─975 /usr/libexec/at-spi2-registryd --use-gnome-session  
│ │ ├─indicator-messages.service  
│ │ │ └─1051 /usr/lib/x86_64-linux-gnu/indicator-messages/indicator-messages-service  
│ │ ├─gvfs-metadata.service  
│ │ │ └─1126 /usr/libexec/gvfsd-metadata  
│ │ ├─dbus.service  
lines 1-37
  ````
   ````
  adrien@node1:/var/log$ journalctl  
-- Logs begin at Wed 2021-10-27 10:49:37 CEST, end at Fri 2021-11-05 18:54:43 CET. --  
oct. 27 10:49:37 node1 kernel: Linux version 5.11.0-38-generic (buildd@lgw01-amd64-041) (gcc (Ubuntu 9.3.0-17u>  
oct. 27 10:49:37 node1 kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-5.11.0-38-generic root=UUID=8770ff9c-ca2>  
oct. 27 10:49:37 node1 kernel: KERNEL supported cpus:
  ````
  ````
adrien@node1:~/Bureau$ sudo nano /etc/vsftpd.conf  
[sudo] Mot de passe de adrien :  
#write_enable=YES --> write_enable=YES
````
![](PNG/Clien_ftp_1.png)  

````
adrien@node1:~/Bureau$ ls  
word.txt
````
upload -->
````
adrien@node1:~/Bureau$ ls  
coucou_je_suis_un_test.txt word.txt
````
````
Mon Nov 8 11:58:00 2021 [pid 2437] [adrien] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/adrien/Bureau/c>  
  
Mon Nov 8 11:57:44 2021 [pid 2418] [adrien] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/adrien/Bureau/cou>
````
````
adrien@node1:/var/log$ cat /etc/vsftpd.conf  
# Make sure PORT transfer connections originate from port 20 (ftp-data).  
connect_from_port_4269=YES
  ````

  
