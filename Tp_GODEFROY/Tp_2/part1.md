
# TP2 : Manipulation de services 

## Nommer la machine 

```
sudo hostname node1.tp2.linux 
sudo nano /etc/hostname 
// Intérieur du fichier /etc/hostname 
node1.tp2.linux
```



## Configuration réseau

**ping 1.1.1.1**

  

```
adrien@node1:~/Bureau$ ping 1.1.1.1 -c 4  
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.  
64 octets de 1.1.1.1 : icmp_seq=1 ttl=54 temps=24.2 ms  
64 octets de 1.1.1.1 : icmp_seq=2 ttl=54 temps=33.0 ms  
64 octets de 1.1.1.1 : icmp_seq=3 ttl=54 temps=28.9 ms  
64 octets de 1.1.1.1 : icmp_seq=4 ttl=54 temps=24.9 ms  
  
--- statistiques ping 1.1.1.1 ---  
4 paquets transmis, 4 reçus, 0 % paquets perdus, temps 3006 ms  
rtt min/avg/max/mdev = 24.159/27.748/33.017/3.535 ms
```
**ping ynov.com**
```
adrien@node1:~/Bureau$ ping [ynov.com](http://ynov.com/) -c 4  
PING [ynov.com](http://ynov.com/) (92.243.16.143) 56(84) bytes of data.  
64 octets de [xvm-16-143.dc0.ghst.net](http://xvm-16-143.dc0.ghst.net/) (92.243.16.143) : icmp_seq=1 ttl=50 temps=21.5 ms  
64 octets de [xvm-16-143.dc0.ghst.net](http://xvm-16-143.dc0.ghst.net/) (92.243.16.143) : icmp_seq=2 ttl=50 temps=21.2 ms  
64 octets de [xvm-16-143.dc0.ghst.net](http://xvm-16-143.dc0.ghst.net/) (92.243.16.143) : icmp_seq=3 ttl=50 temps=22.7 ms  
64 octets de [xvm-16-143.dc0.ghst.net](http://xvm-16-143.dc0.ghst.net/) (92.243.16.143) : icmp_seq=4 ttl=50 temps=22.6 ms  
  
--- statistiques ping [ynov.com](http://ynov.com/) ---  
4 paquets transmis, 4 reçus, 0 % paquets perdus, temps 3004 ms  
rtt min/avg/max/mdev = 21.202/21.996/22.716/0.651 ms
```
**ping <IP_VM>**
```
 C:\Users\adrie\OneDrive\Bureau\ynov\tp-ynov>ping 192.168.56.103

Envoi d’une requête 'Ping'  192.168.56.103 avec 32 octets de données :
Réponse de 192.168.56.103 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.103 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.103 : octets=32 temps<1ms TTL=64
Réponse de 192.168.56.103 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.56.103:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
## Partie 1 : SSH

**[](#1-installation-du-serveur)1. Installation du serveur**
```
adrien@node1:~/Bureau$ sudo apt install openssh-server  
[sudo] Mot de passe de adrien :  
Lecture des listes de paquets... Fait  
Construction de l'arbre des dépendances  
Lecture des informations d'état... Fait  
Les paquets supplémentaires suivants seront installés :  
ncurses-term openssh-sftp-server ssh-import-id  
Paquets suggérés :  
molly-guard monkeysphere ssh-askpass  
Les NOUVEAUX paquets suivants seront installés :  
ncurses-term openssh-server openssh-sftp-server ssh-import-id
```

  **2. Lancement du service SSH**
  
````
adrien@node1:~/Bureau$ systemctl start  
Too few arguments.  
  
adrien@node1:~/Bureau$ systemctl status  
● node1  
State: running  
Jobs: 0 queued  
Failed: 0 units  
Since: Wed 2021-10-27 13:55:26 CEST; 31min ago  
CGroup: /  
├─user.slice  
│ └─user-1000.slice  
│ ├─user@1000.service  
│ │ ├─gvfs-goa-volume-monitor.service  
│ │ │ └─1089 /usr/libexec/gvfs-goa-volume-monitor  
│ │ ├─pulseaudio.service  
│ │ │ └─844 /usr/bin/pulseaudio --daemonize=no --log-target=journal  
│ │ ├─gvfs-daemon.service  
│ │ │ ├─ 949 /usr/libexec/gvfsd  
│ │ │ ├─ 954 /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f -o big_writes  
│ │ │ └─1123 /usr/libexec/gvfsd-trash --spawner :1.8 /org/gtk/gvfs/exec_spaw/0  
│ │ ├─gvfs-udisks2-volume-monitor.service  
│ │ │ └─1069 /usr/libexec/gvfs-udisks2-volume-monitor  
│ │ ├─xfce4-notifyd.service  
│ │ │ └─1044 /usr/lib/x86_64-linux-gnu/xfce4/notifyd/xfce4-notifyd  
│ │ ├─init.scope  
│ │ │ ├─833 /lib/systemd/systemd --user  
│ │ │ └─834 (sd-pam)  
│ │ ├─gvfs-gphoto2-volume-monitor.service  
│ │ │ └─1084 /usr/libexec/gvfs-gphoto2-volume-monitor  
│ │ ├─obex.service  
│ │ │ └─1169 /usr/lib/bluetooth/obexd  
│ │ ├─at-spi-dbus-bus.service  
│ │ │ ├─946 /usr/libexec/at-spi-bus-launcher  
lines 1-30	
````

````
adrien@node1:~/Bureau$ systemctl enable ssh  
Synchronizing state of ssh.service with SysV service script with /lib/systemd/systemd-sysv-install.  
Executing: /lib/systemd/systemd-sysv-install enable ssh
````

**3. Etude du service SSH**

```
adrien@node1:~/Bureau$ ps  
3507 pts/0 00:00:00 ps
```
````
adrien@node1:~/Bureau$ ss -l  
Netid State Recv-Q Send-Q Local Address:Port Peer Address:Port Process  
nl UNCONN 0 0  
tcp LISTEN 0 128 0.0.0.0:ssh 0.0.0.0:*
tcp LISTEN 0 128 [::]:ssh [::]:*
````

````
adrien@node1:/var/log$ journalctl | grep ssh  
oct. 27 10:49:50 node1 systemd[706]: Listening on GnuPG cryptographic agent (ssh-agent emulation).  
oct. 27 10:50:08 node1 systemd[788]: Listening on GnuPG cryptographic agent (ssh-agent emulation).  
oct. 27 10:50:19 node1 systemd[706]: gpg-agent-ssh.socket: Succeeded.
````
**4. Modification de la configuration du serveur**

````
adrien@node1:~/Bureau$ sudo nano /etc/ssh/sshd_config  
adrien@node1:~/Bureau$ cat /etc/ssh/sshd_config  
#Port 4269
````
````
adrien@node1:~/Bureau$ systemctl restart ssh
````