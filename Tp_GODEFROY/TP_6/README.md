


# TP6 : Stockage et sauvegarde

## Partie 1 : Préparation de la machine backup.tp6.linux
````
[AD@backup ~]$ lsblk | grep sdb
sdb 8:16 0 5G 0 disque
````
````
[AD@backup ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[AD@backup ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0 
  /dev/sdb      lvm2 ---   5.00g 5.00g
[AD@backup ~]$ sudo pvdisplay  
[...]
  "/dev/sdb" is a new physical volume of "5.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name               
  PV Size               5.00 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               MVjIIR-pWYt-hx9c-qCNf-7Jav-ossn-xYHZtK
[AD@backup ~]$ sudo vgcreate backup /dev/sdb
  Volume group "backup" successfully created
[AD@backup ~]$ sudo lvcreate -l 100%FREE backup -n lv_backup
  Logical volume "lv_backup" created.
[AD@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/lv_backup
  LV Name                lv_backup
  VG Name                backup
  LV UUID                Sd3fhf-B7if-2iKk-dfRs-CGDS-9Aa1-B4p61T
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-11-30 11:17:04 +0100
  LV Status              available
  # open                 0
  LV Size                <5.00 GiB
  Current LE             1279
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
[AD@backup ~]$ sudo mkfs -t ext4 /dev/backup/lv_backup
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 1309696 4k blocks and 327680 inodes
Filesystem UUID: 71423fc2-8249-457b-ba84-64560341c062
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done 

````
````
[AD@backup ~]$ sudo mkdir /backup
[AD@backup ~]$ sudo mount /dev/backup/lv_backup /backup
[AD@backup ~]$ df -h | grep backup
/dev/mapper/backup-lv_backup  4.9G   20M  4.6G   1% /backup
[AD@backup ~]$ sudo chown AD:AD /backup
[AD@backup ~]$ echo "test" > /backup/test.txt
[AD@backup ~]$ cat /backup/test.txt
test 
[AD@backup ~]$ rm /backup/test.txt
[AD@backup ~]$ cat /etc/fstab | grep backup
/dev/backup/lv_backup /backup ext4 defaults 0 0
[AD@backup ~]$ sudo umount /backup 
[AD@backup ~]$ sudo mount -av
[...]
/backup                  : successfully mounted

````
## Partie 2 : Setup du serveur NFS sur backup.tp6.linux
````
[AD@backup ~]$ mkdir /backup/web.tp6.linux
[AD@backup ~]$ mkdir /backup/db.tp6.linux
````
````
[AD@backup ~]$ sudo dnf install nfs-utils
[...]
Complete!
````
````
[AD@backup ~]$ cat /etc/idmapd.conf | grep Domain
Domain = tp6.linux
[AD@backup ~]$ cat /etc/exports
/backup/web.tp6.linux 10.5.1.11(rw,no_root_squash)
/backup/db.tp6.linux 10.5.1.12(rw,no_root_squash)
````
````
[AD@backup ~]$ sudo systemctl start nfs-server
[AD@backup ~]$ systemctl status nfs-server
● nfs-server.service - NFS server and srvices
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Tue 2021-11-30 12:36:35 CET; 8s ago
[...]
[AD@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[AD@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
[AD@backup ~]$ sudo firewall-cmd --reload
success
[AD@backup ~]$ sudo systemctl restart nfs-server
[AD@backup ~]$ sudo ss -laputen | grep 2049
udp   UNCONN 0      0               0.0.0.0:58519      0.0.0.0:*     ino:62049 sk:18 <->                                                                                
tcp   LISTEN 0      64              0.0.0.0:2049       0.0.0.0:*     ino:62035 sk:1c <->                                                                                
tcp   LISTEN 0      64                 [::]:2049          [::]:*     ino:62048 sk:20 v6only:1 <-
````
## Partie 3 : Setup des clients NFS : web.tp6.linux et db.tp6.linux
````
[AD@web ~]$ sudo dnf install nfs-utils
[...]
Complete!
[AD@web ~]$ sudo mkdir /srv/backup
[AD@web ~]$ cat /etc/idmapd.conf | grep tp6.linux
Domain = tp6.linux
[AD@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux /srv/backup
[AD@web ~]$ df -h | grep backup
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
[AD@web ~]$ echo "inshalla" > /srv/backup/test.txt
[AD@web ~]$ cat /srv/backup/test.txt 
inshalla
[AD@web ~]$ cat /etc/fstab | grep backup
10.5.1.13:/backup/web.tp6.linux /srv/backup nfs defaults 0 0
[AD@web ~]$ sudo umount /srv/backup
[AD@web ~]$ sudo mount -av
[...]
mount.nfs: timeout set for Tue Nov 30 13:20:16 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.5.1.13,clientaddr=10.5.1.11'
/srv/backup              : successfully mounted
````
````
[AD@db ~]$ df -h | grep backup
10.5.1.13:/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
[AD@db ~]$ echo "sah" > /srv/backup/test.txt
[AD@db ~]$ cat /srv/backup/test.txt
sah
[AD@db ~]$ cat /etc/fstab | grep db
10.5.1.13:/backup/db.tp6.linux /srv/backup nfs defaults 0 0
[AD@db ~]$ sudo umount /srv/backup 
[AD@db ~]$ sudo mount -av
[...]
mount.nfs: timeout set for Tue Nov 30 13:31:10 2021
mount.nfs: trying text-based options 'vers=4.2,addr=10.5.1.13,clientaddr=10.5.1.12'
/srv/backup              : successfully mounted
````
## Partie 4 : Scripts de sauvegarde
````

````
