
## Fichier de conf
````
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.200.1.69
NETMASK=255.255.255.0
````
## $ ip a
````
[AD@localhost network-scripts]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:77:0e:ea brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83593sec preferred_lft 83593sec
    inet6 fe80::a00:27ff:fe77:eea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:bf:0e:57 brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.69/24 brd 10.200.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febf:e57/64 scope link
       valid_lft forever preferred_lft forever

````
## sshd
````
[AD@localhost network-scripts]$ systemctl status sshd.service                                           
● sshd.service - OpenSSH server daemon                                                                     
Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)                  
Active: active (running) since Tue 2021-11-23 11:17:20 CET; 55min ago 
````

## Ping

````
[AD@localhost network-scripts]$ ping 8.8.8.8                                                            PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.                                                            
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=53.2 ms                                                  
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=55.6 ms                                                  
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=55.8 ms                                                  
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=23.1 ms 
````

````
[AD@localhost network-scripts]$ ping pornhub.com
PING pornhub.com (66.254.114.41) 56(84) bytes of data.
64 bytes from reflectededge.reflected.net (66.254.114.41): icmp_seq=1 ttl=52 time=74.6 ms
64 bytes from reflectededge.reflected.net (66.254.114.41): icmp_seq=2 ttl=52 time=25.0 ms
64 bytes from reflectededge.reflected.net (66.254.114.41): icmp_seq=3 ttl=52 time=41.4 ms
64 bytes from reflectededge.reflected.net (66.254.114.41): icmp_seq=4 ttl=52 time=63.6 ms
````
## hostname

````
[AD@localhost network-scripts]$ cat /etc/hostname
node1.tp4.linux
````
````
[AD@localhost network-scripts]$ hostname
node1.tp4.linux
````

## Installe & Analyse NGINX

````
$ sudo dnf install nginx
// instalation de nginx
````

````
[AD@localhost network-scripts]$ sudo systemctl start nginx.service
// on lance le sevice nginx
````
````
[AD@localhost network-scripts]$ sudo systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-23 12:38:22 CET; 41s ago
// on verifi quil es start
// (dsl pour les faut dortographe je suis dix)
````
````
[AD@localhost network-scripts]$ sudo systemctl enable nginx.service
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
//fair en sort quil se lance au demarage de la machine
````

````
[AD@localhost network-scripts]$ sudo ps aux| grep nginx
root        8222  0.0  0.0 119160  2160 ?        Ss   12:38   0:00 nginx: master process /usr/sbin/ngin
AD          8263  0.0  0.0 221928  1156 pts/0    S+   12:49   0:00 grep --color=auto nginx
````

````
[AD@node1 ~]$ sudo ss -ltpn | grep nginx
LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=8223,fd=8),("nginx",pid=8222,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=8223,fd=9),("nginx",pid=8222,fd=9))
````

## Visite du service web

````
[AD@node1 nginx]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[AD@node1 nginx]$ sudo firewall-cmd --permanent --zone=public --add-service=https                                                                            
success      
````

````
[AD@node1 nginx]$ sudo firewall-cmd --reload
success
````

````
[AD@node1 nginx]$ curl http://10.200.1.69:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
        font-family: sans-serif, helvetica;
        margin: 0;
        padding: 0;
      }
      :link {
        color: #c00;
      }
      :visited {
        color: #c00;
      }
      a:hover {
        color: #f50;
      }
      h1 {
        text-align: center;
        margin: 0;
        padding: 0.6em 2em 0.4em;
        background-color: #10B981;
        color: #fff;
        font-weight: normal;
        font-size: 1.75em;
        border-bottom: 2px solid #000;
      }
      h1 strong {
        font-weight: bold;
        font-size: 1.5em;
      }
      h2 {
        text-align: center;
        background-color: #10B981;
        font-size: 1.1em;
        font-weight: bold;
        color: #fff;
        margin: 0;
        padding: 0.5em;
        border-bottom: 2px solid #000;
      }
      hr {
        display: none;
      }
      .content {
        padding: 1em 5em;
      }
      .alert {
        border: 2px solid #000;
      }

      img {
        border: 2px solid #fff;
        padding: 2px;
        margin: 2px;
      }
      a:hover img {
        border: 2px solid #294172;
      }
      .logos {
        margin: 1em;
        text-align: center;
      }
      /*]]>*/
    </style>
  </head>

  <body>
    <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>

    <div class="content">
      <p>
        This page is used to test the proper operation of the
        <strong>nginx</strong> HTTP server after it has been installed. If you
        can read this page, it means that the web server installed at this site
        is working properly.
      </p>

      <div class="alert">
        <h2>Website Administrator</h2>
        <div class="content">
          <p>
            This is the default <tt>index.html</tt> page that is distributed
            with <strong>nginx</strong> on Rocky Linux. It is located in
            <tt>/usr/share/nginx/html</tt>.
          </p>

          <p>
            You should now put your content in a location of your choice and
            edit the <tt>root</tt> configuration directive in the
            <strong>nginx</strong>
            configuration file
            <tt>/etc/nginx/nginx.conf</tt>.
          </p>

          <p>
            For information on Rocky Linux, please visit the
            <a href="https://www.rockylinux.org/">Rocky Linux website</a>. The
            documentation for Rocky Linux is
            <a href="https://www.rockylinux.org/"
              >available on the Rocky Linux website</a
            >.
          </p>
        </div>
      </div>

      <div class="logos">
        <a href="http://nginx.net/"
          ><img
            src="nginx-logo.png"
            alt="[ Powered by nginx ]"
            width="121"
            height="32"
        /></a>
        <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>

      </div>
    </div>
  </body>
</html>
````

## Modif de la conf du serveur web

````

````